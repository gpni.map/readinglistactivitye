const express = require('express');

const mongoose = require('mongoose');



const app = express();
const port = 8000;


//Connection
mongoose.connect(`mongodb+srv://gperey:admin123@zuitt-batch197.jvr3p9k.mongodb.net/S35-C1?retryWrites=true&w=majority`, {
	useNewUrlParser: true,
	useUnifiedTopology: true
});


let db = mongoose.connection

db.on('error', console.error.bind(console, "Connection"));



//SCHEMA

const usersSchema = new mongoose.Schema({
	firstName: String,
	lastName: String,
	username: String,
	password: String,
	email: String
})


const productsSchema = new mongoose.Schema({
	name: String,
	description: String,
	price: Number
})


const User = mongoose.model('User', usersSchema);
const Product = mongoose.model('Product', productsSchema);


//MIDDLEWARES
app.use(express.json());
app.use(express.urlencoded({extended: true}));


//ROUTES
//Register a user route
app.post('/register', (req, res) => {
	User.findOne({firstName: req.body.firstName, lastName: req.body.lastName}, (error, result) => {
		if(error){
			return res.send(error)
		} else if (result != null && result.firstName == req.body.firstName && result.lastName == req.body.lastName) {
			return res.send('User exists')
		} else {
			let newUser = new User({
				firstName: req.body.firstName,
				lastName: req.body.lastName,
				username: req.body.username,
				password: req.body.password,
				email: req.body.email
			})

			newUser.save((error, savedUser) => {
				if(error){
					return console.error(error)
				} else {
					return res.status(201).send('New User registered!')
				}
			})
		}
	})
})

app.post('/createProduct', (req, res) => {
	Product.findOne({name: req.body.name}, (error, result) => {
		if(error){
			return res.send(error)
		} else if(result != null && result.name == req.body.name){
			return res.send('Product already exists')
		} else {
			let newProduct = new Product({
				name: req.body.name,
				description: req.body.description,
				price: req.body.price
			})

			newProduct.save((error, savedProduct) =>{
				if(error){
					return console.error(error)
				} else {
					return res.status(201).send('New product added')
				}
			})
		}
	})
})



app.get('/users', (req, res) => {
	User.find({}, (error, result) =>{
		res.send(result)
	})
})

app.get('/products', (req, res) =>{
	Product.find({}, (error, result) =>{
		res.send(result)
	})
})


app.listen(port, () => console.log(`Server is running at port ${port}`))


